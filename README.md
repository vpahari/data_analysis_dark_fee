# data_analysis_dark_fee

The repository shows the data analysis for our dark fee transactions simulations.

## Simulations Background

We are trying to understand the dynamics of a system where transaction issuers can offer their fees as :
(1) Open Fee : offered to all mining pools (all MPOs)
(2) Dark Fee : offered to 1 mining pool (1 MPO)

We assume that every mining pool has a fixed hashrate and makes their prioritization queues public. The queues are sorted based on the feerate offered. Mining pools prioritize transactions based on their expected value as such:

(1) If a transaction offers an open fee of X, then the mining pool has probability of h, where h is its hashrate, of mining this transaction. So its expected value for the mining pool is X * h, and is prioritized as if the transaction offered X*h
(2) If a transaction offers a dark fee of X, then it is prioritized as having offered X.

The transaction issuer is concerned with getting its transaction approved as early as possible. So the expected waiting time is the only factor that it takes into account whether to offer their transaction as open or dark.

## GPS Approximation

In order to calculate the expected waiting time of an incoming transaction, we use a Generalized Processor Sharing (GPS) approximation. It is difficult to calculate the expected waiting time in a system with open transactions that can be mined by any miner and a dark transaction that is only offered to one miner. In order to calculate the exact expected waiting time of an open transaction, we need to go through every possibility of how a transaction can be mined, and its respective probability. 

If a transaction is offered as a :

(1) Dark fee (to 1 miner) : then the expected waiting time is the expected waiting time of the transaction above it plus (1 / h), where h is the hashrate of the miner

(2) Open fee (to all miners): suppose that the expected waiting time of the transaction above in each mining pool queue is t1, t2, ... tn. WLOG, assume it is sorted. Then the expected waiting time is calculated as :

from t1 to t2 : h1 * (t2 - t1) portion of the transaction goes through
from t2 to t3 : (h1 + h2) * (t3 - t2) portion of the transaction goes through

Continue this process until the entire transactions goes through. In our case here, we assume that all transactions have size 1, so the transaction goes through when the process gets to 1.

Now that we have a method of calculating expected waiting time, we can run simualations to see what decsions transaction issuers will take


## Simulations

We make the following simplifying assumptions:

- Transaction size 1
- Block size 1
- Round based simulator with 1 block each round (so no forks)
- Feerate of transaction issuer is sampled from a distribution (Gaussian or Exponential), so congestion does not play a role in determining the feerate
- MPOs order transactions based on feerate
- Transaction issuers are only concerned with getting their transaction approved as early as possible (minimum expected waiting time)

We ran two kinds of simulations:

(1) We let the MPO queues build up without removing any transactions. Here, we are trying to see what decisions a transaction issuer makes as more and more transaction enter the system. Conversely this can also be though about as how the queues of the different MPOs look, when there is suddenly extreme congestion. The results for this can be found in the notebook : data_analysis_dark_simulations_range_dark_fees.ipynb

(2) We start letting transactions through, i.e. miners create blocks and remove the transactions from the top of their queue. Here we are trying to see what fraction of the total fee is earned by each miner. The results for this can be found in the notebook : data_analysis_dark_simulations_miner_fees.ipynb










