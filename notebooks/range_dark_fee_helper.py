import random

SEED = 1000

random.seed(SEED)


#Returns the index of the list with the minimum 
def get_all_index_of_min_random_choose(list_to_use):
    
    indices = [index for index, element in enumerate(list_to_use) if element == min(list_to_use)]
    to_use = random.choice(indices)
    
    return to_use


#Given a list of MPO states and the position that the transaction would be put into as dark fee txs
#Returns the smallest finishing time of the transaction and the index of the MPO it is included in
def expected_wait_time_for_hidden_wfq(MPO_list, hidden_block_list):
    
    counter = 0
    finish_time_list = []
    
    for MPO in MPO_list:
        
        curr_hashrate = MPO.hashrate
        curr_MPO_position = hidden_block_list[counter]
        
        if curr_MPO_position == 0:
            prev_MPO_tx_virFinish = 0
        
        else:
            prev_MPO_tx = MPO.MPO_hidden_queue.queue[curr_MPO_position - 1]
            prev_MPO_tx_virFinish = prev_MPO_tx.virFinish
        
        curr_tx_finish_time = prev_MPO_tx_virFinish + (1 / curr_hashrate)  
        finish_time_list.append(curr_tx_finish_time)     
        counter += 1
        
    MPO_index_with_min_finish_time = get_all_index_of_min_random_choose(finish_time_list)
    min_finish_time = min(finish_time_list)
    
    return min_finish_time, MPO_index_with_min_finish_time


#Given a list of start time, finish time and hashrate, sorted in terms of start time, 
#returns a list where the hashrates for different times is combined
def combine_p_list(p_list):
    
    new_p_list = [p_list[0]]
    
    for i in range(1, len(p_list)):
        
        curr_start_1, curr_finish_1, curr_hashrate_1 = p_list[i]
        last_start_1, last_finish_1, last_hashrate_1 = new_p_list[-1]
                
        if curr_start_1 == last_start_1:
            new_p_list[-1] = (curr_start_1, min(curr_finish_1,last_finish_1), curr_hashrate_1 + last_hashrate_1)
            
        else:
            new_p_list.append((curr_start_1, curr_finish_1, curr_hashrate_1))
            
    return new_p_list


#Given a list of start times, end times and hashrate, 
#returns the expected time that a packet will finish transmitting and the time taken to be transmitted
def calculation_for_open_transaction(p_list):
    
    final_sum = 0
    curr_total_hashrate = 0
        
    for i in range(len(p_list) - 1):
        
        start_1, finish_1, hashrate_1 = p_list[i]
        start_2, finish_2, hashrate_2 = p_list[i + 1]
        curr_total_hashrate += hashrate_1
        
        if final_sum + (start_2 - start_1) * curr_total_hashrate > 1:
            thing_to_return = start_1 + (1 - final_sum) / curr_total_hashrate
            return thing_to_return, thing_to_return - p_list[0][0]
            
        else:
            final_sum += (start_2 - start_1) * curr_total_hashrate
            if round(final_sum,4) == 1:
                return start_2, start_2 - p_list[0][0]
            
    start_i, finish_i, hashrate_i = p_list[-1]   
    end_time = start_i + (1 - final_sum)
    first_start_time = p_list[0][0]
    
    return end_time, end_time - first_start_time


#MPO_list is the list of MPOs with their current queue states
#open_block_list is the block position that the transaction is currently in for each MPOs queue, given it provides its fee as open
#Returns the expected waiting time for a transaction offering their fee as open
def expected_wait_time_for_open_wfq(MPO_list, open_block_list):
    
    counter = 0
    finish_time_list = []
    
    for MPO in MPO_list:
        
        curr_hashrate = MPO.hashrate
        curr_MPO_position = open_block_list[counter]
        
        if curr_MPO_position == 0:
            prev_MPO_tx_virFinish = 0
        
        else:
            prev_MPO_tx = MPO.MPO_hidden_queue.queue[curr_MPO_position - 1]
            prev_MPO_tx_virFinish = prev_MPO_tx.virFinish
        
        curr_tx_finish_time = prev_MPO_tx_virFinish + (1 / curr_hashrate)
        finish_time_list.append((prev_MPO_tx_virFinish, curr_tx_finish_time, curr_hashrate))
        counter += 1
                
    finish_time_list.sort(key=lambda element: (element[0], element[1]))        
    p_list_to_use = combine_p_list(finish_time_list)
    final_open_expected_finish, virtual_size = calculation_for_open_transaction(p_list_to_use)
    
    return final_open_expected_finish, virtual_size


#Given a list of MPO queues, transaction, and size of each block
#Returns the positions at which the transaction would be included in each MPO if fee is offered as open or dark
def get_open_and_hidden_block_list_wfq(MPO_list, tx, size_of_block):
    
    hidden_tx_feerate = tx.hidden_feerate
    open_fee_block_list = []
    hidden_fee_block_list = []
    
    for MPO in MPO_list:
        
        open_tx_feerate = MPO.hashrate * hidden_tx_feerate
        MPO_queue_pos_open = MPO.get_hidden_queue_position_with_feerate(open_tx_feerate)
        open_fee_block_list.append(MPO_queue_pos_open)
        MPO_queue_pos_hidden = MPO.get_hidden_queue_position_with_feerate(hidden_tx_feerate)
        hidden_fee_block_list.append(MPO_queue_pos_hidden)
        
    return open_fee_block_list, hidden_fee_block_list


    
    

